﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Lesson3_Reflection
{
    /// <summary>
    /// Кастомный сериализатор реализует IFormatter
    /// Собственный суррогат селектор позволяет производить сериализацию объекта, 
    /// не помеченного атрибутом Serialized
    /// </summary>
    public class FooSerializer : IFormatter
    {
        public ISurrogateSelector SurrogateSelector { get; set; }
        
        public SerializationBinder Binder { get ; set ; }
        
        public StreamingContext Context { get ; set ; }
        
        public FooSerializer()
        {
            this.Context = new StreamingContext(StreamingContextStates.All);

            var ss = new SurrogateSelector();
            ss.AddSurrogate(typeof(Foo), this.Context, new FooSerializationSurrogate());

            this.SurrogateSelector = ss;

            this.Binder = new CustomBinder();
        }
        
        public void Serialize(Stream serializationStream, object graph)
        {
            var foo = graph as Foo;
            if (foo == null)
                throw new ArgumentException("Ожидался тип Foo");

            var fooFields = foo.GetType().GetFields();
            
            //Создается словарь, в который будут записаны поля объекта и их значения
            //Что бы показать, что мы умеем читать данные типа через рефлексию
            var fieldsContainer = new Dictionary<string, int>();
            for (int i = 0; i < fooFields.Length; i++)
            {
                fieldsContainer.Add(fooFields[i].Name, (int)fooFields[i].GetValue(foo));
            }

            
            BinaryFormatter bf = new BinaryFormatter();
            bf.Binder = this.Binder;
            bf.SurrogateSelector = this.SurrogateSelector;

            //На самом деле сериализовать будем словарь
            bf.Serialize(serializationStream, fieldsContainer);
        }
        
        public object Deserialize(Stream serializationStream)
        {
            var bf = new BinaryFormatter();
            
            bf.Binder = this.Binder;
            
            bf.SurrogateSelector = new SurrogateSelector();

            return bf.Deserialize(serializationStream);
        }
    }

    /// <summary>
    /// Тип, который позволяет сериализовывать Foo, не помеченный Serialized
    /// </summary>
    public sealed class FooSerializationSurrogate : ISerializationSurrogate
    {
        // Serialize the Employee object to save the object's name and address fields.
        public void GetObjectData(Object obj,
            SerializationInfo info, StreamingContext context)
        {
            var foo = (Foo)obj;
            info.AddValue("i1", foo.i1);
            info.AddValue("i2", foo.i2);
            info.AddValue("i3", foo.i3);
            info.AddValue("i4", foo.i4);
            info.AddValue("i5", foo.i5);
        }

        // Deserialize the Employee object to set the object's name and address fields.
        public Object SetObjectData(Object obj,
            SerializationInfo info, StreamingContext context,
            ISurrogateSelector selector)
        {

            var foo = (Foo)obj;
            foo.i1 = info.GetInt32("i1");
            foo.i2 = info.GetInt32("i2");
            foo.i3 = info.GetInt32("i3");
            foo.i4 = info.GetInt32("i4");
            foo.i5 = info.GetInt32("i5");
            
            return foo;
        }
    }

    /// <summary>
    /// Кастомная реализация Bindera как буд то мы можем ожидать различные версии типа Foo
    /// Особенно не разбирался и просто скопировал код с MSDN
    /// </summary>
    sealed class CustomBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type typeToDeserialize = null;

            // For each assemblyName/typeName that you want to deserialize to
            // a different type, set typeToDeserialize to the desired type.
            String assemVer1 = Assembly.GetExecutingAssembly().FullName;
            String typeVer1 = "Version1Type";

            if (assemblyName == assemVer1 && typeName == typeVer1)
            {
                // To use a type from a different assembly version,
                // change the version number.
                // To do this, uncomment the following line of code.
                // assemblyName = assemblyName.Replace("1.0.0.0", "2.0.0.0");

                // To use a different type from the same assembly,
                // change the type name.
                typeName = "Version2Type";
            }

            // The following line of code returns the type.
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
                typeName, assemblyName));

            return typeToDeserialize;
        }
    }

    //Тестируемый класс данных
    //Не помечаем класс аттрибутом Serializable
    public class Foo
    {
        public int i1, i2, i3, i4, i5;

        public static Foo Get() => new Foo() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

        public void Display()
        {
            Console.WriteLine($"Десериализованный объект имеет поля i1: {i1}, i2: {i2}, i3: {i4}, i4: {i4}, i5: {i5}");
        }
    }
}
