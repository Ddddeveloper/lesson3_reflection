﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;

namespace Lesson3_Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new App();
            app.AppStart();
        }
    }

    public class App
    {
        private Foo foo;
        private bool isWork;
        private DateTime start;
        private DateTime end;

        public App()
        {
            this.foo = Foo.Get();
            this.isWork = true;
        }

        public void AppStart()
        {
            while (isWork)
            {
                Console.Clear();
                Console.WriteLine("Для демонстрации работы BinnaryFormatter нажмите 1");
                Console.WriteLine("Для демонстрации работы FooSerializer нажмите 2");
                Console.WriteLine("Для завершение работы нажмите 3");

                var input = Console.ReadKey().KeyChar.ToString();
                Console.WriteLine();
               
                int val;
                
                if (!Int32.TryParse(input, out val))
                {
                    Console.Clear();
                    Console.WriteLine("Введите число от 1 до 3");
                }
                else if (val > 3)
                {
                    Console.Clear();
                    Console.WriteLine("Введите число от 1 до 3");
                }
                else if (val == 3)
                {
                    this.ShutDown();
                    break;
                }
                else
                {
                    //Будет использован BinnaryFormatter
                    if (val == 1)
                    {
                        var formatter = new BinaryFormatter();
                        
                        //Суррогат для сериализации типа Foo, не отмеченного Serialized
                        var ss = new SurrogateSelector();
                        ss.AddSurrogate(typeof(Foo),  new StreamingContext(StreamingContextStates.All), new FooSerializationSurrogate());
                        formatter.SurrogateSelector = ss;
                        
                        this.ShowSerializerWork(formatter);
                    }
                    else
                    {
                        //Будет исполозьвана кастомная сериализация
                        this.ShowSerializerWork(new FooSerializer());
                    }
                }

                Console.WriteLine("Нажмите любую кнопку, что бы продолжить");
                Console.ReadKey();
            }
        } 
        
        private void ShowSerializerWork(IFormatter formatter)
        {
            var fs = new FileStream(Path.Combine(Environment.CurrentDirectory, "data.dat"), FileMode.Create, FileAccess.ReadWrite);
            try
            {
                this.start = DateTime.Now;

                formatter.Serialize(fs, this.foo);

                this.end = DateTime.Now;
            }
            
            finally
            {
                Console.WriteLine($"Объект типа Foo сериализован за {this.end - this.start}");
                Console.WriteLine();
                fs.Close();
            }

            var fs1 = new FileStream(Path.Combine(Environment.CurrentDirectory, "data.dat"), FileMode.Open, FileAccess.Read);
            object data = null;
            try
            {
                this.start = DateTime.Now;

                data = formatter.Deserialize(fs1);

                this.end = DateTime.Now;
            }

            finally
            {
                Console.WriteLine($"Объект десериализован за {this.end - this.start}");
                Console.WriteLine();

                //Если на выходе получили Foo, применялся BinnaryFormatter
                if (data is Foo)
                {
                    Console.WriteLine("Использован BinnaryFormatter");
                    (data as Foo).Display();
                }
                //Если на выходе получили словарь, применялся FooSerializer
                else if (data is Dictionary<string, int>)
                {
                    Console.WriteLine("Использован кастомный сериализатор, поля объкта сохранялись в словарь");
                    foreach (var field in data as Dictionary<string, int>)
                    {
                        Console.WriteLine($"Поле: {field.Key} имеет значение: {field.Value}");
                    }
                }
                else
                {
                    Console.WriteLine("Но что то пошло не так");
                }

                fs1.Close();
            }
        }

        private void ShutDown()
        {
            Console.WriteLine();
            Console.WriteLine("Работа программы будет завершена через 3 сек");
            Thread.Sleep(1000);
            Console.WriteLine("Работа программы будет завершена через 2 сек");
            Thread.Sleep(1000);
            Console.WriteLine("Работа программы будет завершена через 1 сек");
            Thread.Sleep(1000);
            this.isWork = false;
        }
    }
}
